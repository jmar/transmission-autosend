#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import os
import sys

import pika

if len(sys.argv) >= 2:
    filename = sys.argv[1]
else:
    filename = os.environ.get('TR_TORRENT_NAME', None)

if filename is None:
    print("Unable to find torrent name")
    sys.exit(1)

ini_path = os.path.dirname(os.path.realpath(__file__))

config = configparser.ConfigParser()
config.read('{}/amq_sender.ini'.format(ini_path))

amq_user = config.get('rabbitmq', 'amq_user')
amq_pass = config.get('rabbitmq', 'amq_pass')
amq_host = config.get('rabbitmq', 'amq_host')
amq_port = config.get('rabbitmq', 'amq_port')
amq_vhost = config.get('rabbitmq', 'amq_vhost')
amq_queue = config.get('rabbitmq', 'amq_queue')

credentials = pika.PlainCredentials(amq_user, amq_pass)
connection = pika.BlockingConnection(pika.ConnectionParameters(amq_host, amq_port, amq_vhost, credentials))
channel = connection.channel()

channel.queue_declare(queue=amq_queue)

channel.basic_publish(exchange='',
                      routing_key='transmission',
                      body='{}'.format(filename))

print(" [x] Send '{}'".format(filename))
connection.close()
