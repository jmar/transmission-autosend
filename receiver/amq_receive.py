#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import configparser
import os
import subprocess

import pika
from shlex import quote, split


def upload_file(filename, config):
    """
    Upload a file somewhere
    :param filename: Name of the file
    :return: Boolean
    """

    local_dir = config.get('sftp', 'localDir')
    remote_dir = config.get('sftp', 'remoteDir')
    if not local_dir.endswith('/'):
        local_dir += '/'

    if not remote_dir.endswith('/'):
        remote_dir += '/'

    username = config.get('sftp', 'user')
    hostname = config.get('sftp', 'hostname')
    port = config.get('sftp', 'port')

    command = "scp -i ./id_rsa -P {port} -r {LocalFilePath} {username}@{hostname}:{remoteDir}".format(port=port,
                                                                                                         LocalFilePath=quote(
                                                                                                             local_dir + filename),
                                                                                                         username=username,
                                                                                                         hostname=hostname,
                                                                                                         remoteDir=quote(
                                                                                                             remote_dir))

    try:
        subprocess.Popen(command, shell=True)
        return True
    except Exception as err:
        print(err)
        return False
    return False


def main(config):
    """
    main function
    :param config:
    :return:
    """
    amq_user = config.get('rabbitmq', 'amq_user')
    amq_pass = config.get('rabbitmq', 'amq_pass')
    amq_host = config.get('rabbitmq', 'amq_host')
    amq_port = config.get('rabbitmq', 'amq_port')
    amq_vhost = config.get('rabbitmq', 'amq_vhost')
    amq_queue = config.get('rabbitmq', 'amq_queue')

    credentials = pika.PlainCredentials(amq_user, amq_pass)
    connection = pika.BlockingConnection(pika.ConnectionParameters(amq_host, amq_port, amq_vhost, credentials))
    channel = connection.channel()

    channel.queue_declare(queue=amq_queue)

    print("Listen to queue {} on {}".format(amq_queue, amq_host))
    for method_frame, properties, body in channel.consume('transmission'):

        if upload_file(body.decode("utf-8"), config):
            channel.basic_ack(method_frame.delivery_tag)
            print(' [x] File {} uploaded with success'.format(body.decode('utf-8')))
        else:
            print(' [-] Failed to send file {}, requeue it'.format(body))
            channel.basic_reject(method_frame.delivery_tag, requeue=True)


if __name__ == '__main__':
    ini_path = os.path.dirname(os.path.realpath(__file__))
    config = configparser.ConfigParser()
    config.read('{}/amq_receive.ini'.format(ini_path))

    # logger = logging.basicConfig(format='%(asctime)s :: %(levelname)s :: %(module)s :: %(message)s',
    #                              level=logging.INFO)

    print("[x] Service amqp receiver started")
    main(config)
