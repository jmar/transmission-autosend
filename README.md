# transmission-autosend

Suite de script pour envoyer dans RabbitMQ le nom d'un fichier téléchargé par transmission puis via le receiver, transférer ce fichier sur un serveur sftp distant.

## sender

Ce script est à faire exécuter par Transmission à la fin du téléchargement de votre iso linux préféré.

Le nom du fichier téléchargé est envoyé dans une queue RabbitMQ.

La queue créée et utilisée est `transmission`.

### utilisation

```bash
pipenv --python 3.6
pip install -r requirements.txt
./amq_sender.py filename
```


## receiver

Ce script écoute la queue `transmission` et upload le fichier dont le nom est dans le "body" du message vers un service SFTP distant

### Configuration

La configuration se fait dans le fichier `amq_receive.ini`

* **hostname**: Nom ou IP du serveur sftp distant 
* **user**: Nom d'utilisateur pour se connecter sur le serveur sftp
* **port**: Port distant
* **localDir**: Répertoire local où se trouvent les fichiers téléchargés
* **remoteDir**: Répertoire distant où copier les fichiers

### Utilisation

```bash
pipenv --python 3.6
pip install -r requirements.txt
./amq_receive.py
``` 
